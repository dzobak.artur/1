﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Hotel
    {
        public int RoomNumber { get; set; }
        public int Floor { get; set; }
        public int Capacity { get; set; }
        public decimal DailyRate { get; set; }
    }


    class Tenant
    {
        public string LastName { get; set; }
        public DateTime CheckInDate { get; set; }
        public int RoomNumber { get; set; }
        public DateTime CheckOutDate { get; set; }
    }


    class HotelManager
    {
        private List<Hotel> hotels;
        private List<Tenant> tenants;

        public HotelManager()
        {

            hotels = new List<Hotel>
        {
            new Hotel { RoomNumber = 1, Floor = 1, Capacity = 2, DailyRate = 100 },
            new Hotel { RoomNumber = 2, Floor = 1, Capacity = 3, DailyRate = 150 },
            new Hotel { RoomNumber = 3, Floor = 1, Capacity = 3, DailyRate = 140 },
            new Hotel { RoomNumber = 4, Floor = 2, Capacity = 4, DailyRate = 145 },
            new Hotel { RoomNumber = 5, Floor = 3, Capacity = 4, DailyRate = 140 },
            new Hotel { RoomNumber = 6, Floor = 3, Capacity = 4, DailyRate = 140 },
            new Hotel { RoomNumber = 7, Floor = 3, Capacity = 2, DailyRate = 100 },
            new Hotel { RoomNumber = 8, Floor = 4, Capacity = 3, DailyRate = 150 },
            new Hotel { RoomNumber = 9, Floor = 4, Capacity = 3, DailyRate = 140 },
            new Hotel { RoomNumber = 10, Floor = 5, Capacity = 4, DailyRate = 145 },
            new Hotel { RoomNumber = 11, Floor = 5, Capacity = 4, DailyRate = 140 },
            new Hotel { RoomNumber = 12, Floor = 5, Capacity = 4, DailyRate = 140 },

        };

            tenants = new List<Tenant>
        {
            new Tenant { LastName = "Smith", RoomNumber = 1, CheckInDate = new DateTime(2022, 5, 15), CheckOutDate = new DateTime(2023, 5, 20) },
            new Tenant { LastName = "Johnson", RoomNumber = 2, CheckInDate = new DateTime(2022, 12, 10), CheckOutDate = new DateTime(2023, 4, 5) },
            new Tenant { LastName = "Lamb", RoomNumber = 2, CheckInDate = new DateTime(2022, 4, 10), CheckOutDate = new DateTime(2022, 9, 6) },
            new Tenant { LastName = "Evans", RoomNumber = 2, CheckInDate = new DateTime(2022, 3, 10), CheckOutDate = new DateTime(2022, 12, 7) },
            new Tenant { LastName = "Alexander", RoomNumber = 3, CheckInDate = new DateTime(2022, 12, 10), CheckOutDate = new DateTime(2023, 12, 6) },
            new Tenant { LastName = "Rowe", RoomNumber = 3, CheckInDate = new DateTime(2021, 1, 8), CheckOutDate = new DateTime(2024, 9, 12) },
            new Tenant { LastName = "Ford", RoomNumber = 1, CheckInDate = new DateTime(2020, 4, 1), CheckOutDate = new DateTime(2025, 11, 14) },
            new Tenant { LastName = "Paul", RoomNumber = 1, CheckInDate = new DateTime(2022, 2, 3), CheckOutDate = new DateTime(2023, 12, 17) },
            new Tenant { LastName = "Turner", RoomNumber = 3, CheckInDate = new DateTime(2021, 4, 6), CheckOutDate = new DateTime(2022, 11, 16) },
            new Tenant { LastName = "Peters", RoomNumber = 4, CheckInDate = new DateTime(2022, 6, 7), CheckOutDate = new DateTime(2022, 10, 24) },
            new Tenant { LastName = "Wang", RoomNumber = 5, CheckInDate = new DateTime(2021, 7, 4), CheckOutDate = new DateTime(2022, 10, 21) },
            new Tenant { LastName = "Davis", RoomNumber = 4, CheckInDate = new DateTime(2021, 8, 1), CheckOutDate = new DateTime(2022, 10, 16) },
            new Tenant { LastName = "Paulyuias", RoomNumber = 5, CheckInDate = new DateTime(2022, 2, 3), CheckOutDate = new DateTime(2023, 12, 17) },
            new Tenant { LastName = "Turnerwef", RoomNumber = 5, CheckInDate = new DateTime(2021, 4, 6), CheckOutDate = new DateTime(2022, 11, 16) },
            new Tenant { LastName = "Peterssdf", RoomNumber = 4, CheckInDate = new DateTime(2022, 6, 7), CheckOutDate = new DateTime(2022, 10, 24) },
            new Tenant { LastName = "Wangqwr", RoomNumber = 3, CheckInDate = new DateTime(2021, 7, 4), CheckOutDate = new DateTime(2022, 10, 21) },
            new Tenant { LastName = "Davissdf", RoomNumber = 6, CheckInDate = new DateTime(2021, 8, 1), CheckOutDate = new DateTime(2022, 10, 16) },
        };
        }

        public void TaskA()
        {      
            int roomNumberToSearch = 4; 
            int currentYear = DateTime.Now.Year;

            var averageStayDuration = tenants
                .Where(tenant => tenant.RoomNumber == roomNumberToSearch &&
                                 tenant.CheckInDate.Year == currentYear - 1)
                .Average(tenant => (tenant.CheckOutDate - tenant.CheckInDate).TotalDays);

            Console.WriteLine($"Середня тривалість проживання у кімнаті {roomNumberToSearch} минулого року: {averageStayDuration} днів");
        }

        public void TaskB()
        {           
            var floorsWithFewestRooms = hotels
                .GroupBy(hotel => hotel.Floor)
                .OrderBy(group => group.Count())
                .First();

            Console.WriteLine("Поверхи з найменшою кількістю кімнат:");
            foreach (var floorGroup in floorsWithFewestRooms)
            {
                Console.WriteLine($"Поверх {floorGroup.Floor}");
            }
        }

        public void TaskC()
        {
            DateTime currentDate = DateTime.Now;
            var tenantBills = tenants
                //.Where(tenant => tenant.CheckOutDate >= currentDate)
                .Select(tenant =>
                {
                    var hotel = hotels.FirstOrDefault(h => h.RoomNumber == tenant.RoomNumber);
                    if (hotel != null)
                    {
                        TimeSpan stayDuration = currentDate - tenant.CheckInDate;
                        double totalDays = stayDuration.TotalDays;
                        decimal totalAmount = (decimal)totalDays * hotel.DailyRate;
                        return new
                        {
                            tenant.LastName,
                            TotalAmount = totalAmount
                        };
                    }
                    return null;
                })
                .Where(bill => bill != null);

            Console.WriteLine("Рахунки для жильців, які ще не виселилися:");
            foreach (var bill in tenantBills)
            {
                Console.WriteLine($"{bill.LastName}: {bill.TotalAmount:C}");
            }
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            HotelManager manager = new HotelManager();
            manager.TaskA();
            manager.TaskB();
            manager.TaskC();

        }
    }
}
